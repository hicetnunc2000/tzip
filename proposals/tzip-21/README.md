---
tzip: TZIP21
title: Smart Contract Public Fabric 
author: Rafael Lima (@hicetnunc2000)
gratuity: tz1UBZUkXpKGhYsP5KtzDNqLLchwF4uHrGjw
status: Work In Progress
type: Interface
created: 2020-10-12
---


## Summary

This interoperability proposal uses the michelson feature CREATE_CONTRACT to generate public interfaces on-chain.

## Abstract

Such interface provides accessibility for eachever user or smart contract who whishes to publish an FA1.2 token, and possibly other standards, as it can be implemented in different standards. 

## Motivation

dApp developers wouldn't need to deal with storages for deploying such a contract from a public fabric.

## Rationale

This interface must request the partial or the total storage of the smart contract for an user.

## Implementations

- Mainnet [KT1JkE4T6umrTh15kKSyJ8cLjNu2cdd6QtNj](https://better-call.dev/mainnet/KT1JkE4T6umrTh15kKSyJ8cLjNu2cdd6QtNj) 
- [SmartPy](https://www.smartpy.io/dev/index.html?code=eJzNWN1v2zYQf_dfwSlAQ3WO3Bjowwp0mNstQB_WDVu6lyAQaIm2hUqkQNJN3a7923f8kERK8kfcDZgRJBF13_e74_ku0M2WrYtlSdFCSqokukI3i@v55AK9YbIuBM3Rcoc2StXyxWy2LlRJlknGq5n6VNT217Lky1lFpKJitpgBdzJPqnwyKaqaC4VkRYSqd4hIJOvJJCuJlEZHuiJLUWRY1slrzpQgmYpfTBB8crpCaVqwQqUplrRcuXP90Y9JJihRYNpLI0hLIHkuqJT4Un26voxjdJFvqwqU2uOQWwvG7ZH@cFGsC@ZEfv4SvIvbJ_PPT6CMgrW7tOYFU629rYjUmgSKpqgmglTSs_69AgUgoeQZKfHl5RQNbfeIkw@k3FLLInlFQ6vh0AYizVz8wveGRnFB1k6EoBkX@ZBIf0heQVwkiAEOILemT0dpa7KVNvqklHScZklKwjIqreZlsU4rUuPPX@JxcsUVKf_c1nW5A45nozTxFDWOaqkeEOI44TVlqYlRPMocQiAniiRd1u@aUN@3nndZ9xAL4aOn4HWKnj6lH4EkdfHvA9hAcDxEysv57R82Y6SuBYdzR3r7K9DpvwuLHIOi27dEQYScUEeoz@CwF92heZOD2AZKJldU7EO1RidVqdrVFDvc@NavBK9SZ1BrseKDI99xbXhSkh3fKowjKwE6iP4nmiIcaXZ4VhyeIsMYxbFXOyDjAxXFaodtvGUaABwbk1lORYz@DvCCvzYMFuU4Rk8GgMLOzcS5Zgt0VJ6l70DXJP3Ol3CftBm@ayXdox8bMFpwhg5qidA5XPTerN5SkCqJ2DW2QYj2xuOQKQ2C@tpHKuiIBBMWIlOosdPVXj1aKzjasX8fWu0HoFihMG_fBXl74uV@H1g80J8UidG0PiosoxL6MTpYvVYCPbN4ZW10nl6rjiGoyxEYDsqsIyIldPV8t7B2502vD6PURaMLUbKmqsmwM2OKno1pxwMd0BahdFuAOP_0KTjyjkmyoouy5A9a@@sNYWtw7BA2x@y7C23rLhuH1S5lo3VtE@imhTCDAO2vI1Yk@r4kBZN4yLXHbkd4H0wMXUHD3eFfRnCfd@D7UNAH3ACi9QVy8sqyH0AgqNyWB6qhre9T1LV5@gaFCX9gB3N3iiG33bX72Op7BwNCfNBg7053tlwgtSkkgh_GlValEIfLe0NhCiQsJwJm@a1CGWFoCWfACvM5YF9tBN@uN6hgG6gOpcOQtG50FdobxAVVW8HM5MJ5ic0o@B8b0uvKJqKuMR@1y5viQIhKl0CE26HO8e9roZrjzP7p6ulY_zxncnnMMNDU_wnXadMBjl@pY1gcUB8MrMnC_zmwJ4xNg3idMTgNZJw2IwzYjg9P4VeBMSU@xWDQ8MoojF6_lM4pWTdp@Pd9wH8YSwCWxYi2k3G1GJTIuZU4tLz7Wjm8NQK9zRX2LY4cvzsC28Ksmma_L5uHb4JWvCU7mq3fjapHOvcKmvq_laJ2j9HkposDDm@K6Qjkp17ApmgsYr2dgOH2XGxZkh5hb78y2M1YQY25fwGQqDhtLaFGFxEgxu2lGGf9rmH4TRq85ttx3P5Wq4IzrJoxf@8SgQg9nu9Ld5uUTrTZ5jjKyQRG3EjRqi6JojIyk0XBwD9GKpqm3f0NPTEFCoX1CxAU6TBHXXWZd6C5U51RRkTBrVL9Om2OvF1Sc5RsrnFklpyosUbvTJstKjFb1Cgeik8UAYKUr8y2DiIksUd10eomWca3TEEXgAFU@4p@@Xn@_Pn1D@g93V3VpBAS_FBUWEwUGSnLXRdIfSV4njhpOAo6yqh92rGFJZfRiONywx@w@X5wv4@9AaDHnl03W1rN2c0hIwLmTWBvzGLYFzJ3Qpqd8dA8PXdk8z3HSW87G5iSiK2eQXSXeKnPwLZ_ACQsRpw-)

## Appendix/References

- [TZIP-7](https://gitlab.com/tzip/tzip/-/blob/master/proposals/tzip-7/tzip-7.md)
- [SmartPy FA1.2 Template](https://www.smartpy.io/dev/index.html?template=FA1.2.py)

## Copyright

All TZIPs must be in the public domain, or a under a permissive license
substantially identical to placement in the public domain. Example of a
copyright waiver:

Copyright and related rights waived via
[CC0](https://creativecommons.org/publicdomain/zero/1.0/).
